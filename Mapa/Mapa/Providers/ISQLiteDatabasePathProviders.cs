﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mapa.Providers
{
    public interface ISQLiteDatabasePathProviders
    {
        string GetDatabasePath();
    }
}
