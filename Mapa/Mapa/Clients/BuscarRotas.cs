﻿using Mapa.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.GoogleMaps;

namespace Mapa.Clients
{
    class BuscarRotas
    {
        const string Key = "AIzaSyCc7W6c6OsnfhsWFNNBNn6JDb7gRRIg3Yk";

        const string Url = "https://maps.googleapis.com/maps/api/directions/json?";

        public async Task<DirecaoResposta> ObtemRotasAsync(Position origem, Position destino)
        {
            if (origem == null)
                throw new InvalidOperationException("Informe a Origem");

            if (destino == null)
                throw new InvalidOperationException("Informe o Destino");
                       
            string enderecoUrl = $"{Url}origin={origem.Latitude.ToString().Replace(",", ".")}," +
                $"{origem.Longitude.ToString().Replace(",", ".")}" +
                $"&destination={destino.Latitude.ToString().Replace(",", ".")}," +
                $"{destino.Longitude.ToString().Replace(",", ".")}&key={Key}";

            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(enderecoUrl))
                {
                    if (!response.IsSuccessStatusCode)
                        throw new InvalidOperationException("Falha na requisição!");

                    var result = await response.Content.ReadAsStringAsync();

                    if (string.IsNullOrWhiteSpace(result))
                        throw new InvalidOperationException("Falha na conversão!");

                    return JsonConvert.DeserializeObject<DirecaoResposta>(result);
                }
            }
        }
    }
}
