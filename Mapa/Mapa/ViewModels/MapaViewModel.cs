﻿using Mapa.Clients;
using Mapa.Control;
using Mapa.CustomRender;
using Plugin.Geolocator;
using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace Mapa.ViewModels
{
    class MapaViewModel : ViewModelBase
    {
        public MapaViewModel(Position origem, Position destino, bool hist)
        {
            map = new MapCustomRender();
            localizacao = new Localizacao();
            posicaoDestino = destino;
            posicaoOrigem = origem;
            historico = hist;
            AtualizaMapa();
        }

        private string _Titulo;
        public string Titulo
        {
            get => _Titulo;
            set
            {
                _Titulo = value;
                OnPropertyChanged();
            }
        }

        private bool historico;
        private Localizacao localizacao;
        private Position posicaoOrigem;
        private Position posicaoDestino;
        public static MapCustomRender map;

        private async void AtualizaMapa()
        {
            try
            {

                map.PosicaoOrigem = posicaoOrigem;
                Task<string> enderecoOrigem = localizacao.GetDescricaoEnderecoAsync(posicaoOrigem);
                Task<string> enderecoDestino = localizacao.GetDescricaoEnderecoAsync(posicaoDestino);

                BuscarRotas buscar = new BuscarRotas();
                var resultado = await buscar.ObtemRotasAsync(posicaoOrigem, posicaoDestino);

                Pin pinDestino = new Pin()
                {
                    Type = PinType.Place,
                    Position = posicaoDestino,
                    Label = enderecoDestino.Result
                };

                pinDestino.IsDraggable = true;
                map.Pins.Add(pinDestino);

                double tempoSegundos = 0;
                double distanciaMetros = 0;

                foreach (var rota in resultado.Rotas)
                {
                    var posicoes = rota.Polyline.Positions.Select(l => new Position(l.Latitude, l.Longitude)).ToList();
                    tempoSegundos = rota.Legs.Select(l => l.Duration.Value).Sum();
                    distanciaMetros = rota.Legs.Select(l => l.Distance.Value).Sum();

                    map.CoordenadasRota = posicoes;
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(posicoes.First(), Distance.FromKilometers(2)));
                }

                #region Configura titulo

                var maplocator = CrossGeolocator.Current;
                maplocator.DesiredAccuracy = 1;

                tempoSegundos = tempoSegundos / 60;              

                if (distanciaMetros > 1000)
                {
                    distanciaMetros = distanciaMetros / 1000;
                    Titulo = Titulo + $"{distanciaMetros} Km";
                }
                else
                {
                    Titulo = Titulo + $"{distanciaMetros} Mts";
                }

                Titulo = Titulo + " - ";

                if (tempoSegundos > 60)
                {
                    tempoSegundos = tempoSegundos / 60;
                    Titulo = $"{tempoSegundos} Hrs";
                }
                else
                {
                    Titulo = $"{tempoSegundos} Min";
                }


                OnPropertyChanged(nameof(Titulo));

                #endregion

            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Erro", e.Message, "OK");
            }
        }
    }
}
