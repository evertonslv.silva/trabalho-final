﻿using Mapa.Control;
using Mapa.Data;
using Mapa.Data.Dtos;
using Mapa.Views;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace Mapa.ViewModels
{
    class HistoricoViewModel : ViewModelBase
    {
        public HistoricoViewModel() : base()
        {
            foreach (CepDto cep in DataBase.Current.CepGetAll())
            {
                Historicos.Add(new Historico(cep));
            }
        }

        public ObservableCollection<Historico> Historicos { get; private set; } = new ObservableCollection<Historico>();
        private Historico historico;

        #region Selecionar Historico

        private Historico _HistoricoSelecionado { get; set; }
        public Historico HistoricoSelecionado
        {
            get => _HistoricoSelecionado;
            set
            {
                if (_HistoricoSelecionado != value)
                {
                    _HistoricoSelecionado = value;
                }
            }
        }

        #endregion

        #region Navegar pelo Mapa

        private Command _NavegarMapaCommand { get; set; }
        public Command NavegarMapaCommand => _NavegarMapaCommand ?? (_NavegarMapaCommand = new Command(async () => await NavegarMapaCommandExecute()));

        async Task NavegarMapaCommandExecute()
        {
            historico = HistoricoSelecionado;
            Position posOrigem = new Position(historico.cep.LatitudeOrigem, historico.cep.LongitudeOrigem);
            Position posDestino = new Position(historico.cep.LatitudeDestino, historico.cep.LongitudeDestino);

            await PushAsync(new MapaPage(posOrigem, posDestino, true));
        }

        #endregion

        #region Apagar CEP

        private Command _ApagarHistoricoCommand { get; set; }
        public Command ApagarHistoricoCommand => _ApagarHistoricoCommand ?? (_NavegarMapaCommand = new Command(async () => await ApagarHistoricoCommandExecute()));

        async Task ApagarHistoricoCommandExecute()
        {
            DataBase.Current.Delete(HistoricoSelecionado.cep);
            Historicos.Remove(HistoricoSelecionado);
            await App.Current.MainPage.DisplayAlert("", "CEP apagado!", "OK");
        }

        #endregion
    }


}
