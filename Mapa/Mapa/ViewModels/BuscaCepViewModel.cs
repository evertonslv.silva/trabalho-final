﻿using Mapa.Data;
using Mapa.Data.Dtos;
using Mapa.Views;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Essentials;
using System.Linq;
using Mapa.Control;
using Xamarin.Forms.GoogleMaps;

namespace Mapa.ViewModels
{
    class BuscaCepViewModel : ViewModelBase
    {
        public BuscaCepViewModel() : base()
        {
            localizacao = new Localizacao();
            posicaoOrigem = localizacao.GetPosicaoAtualAsinc();
        }

        #region Propriedades

        private CepDto _cepDto = null;
        private Localizacao localizacao = null;
        private Task<Position> posicaoOrigem;
        private Position posicaoDestino;

        private string _EnderecoBusca;
        public string EnderecoBusca
        {
            get => _EnderecoBusca;
            set
            {
                _EnderecoBusca = value;
                OnPropertyChanged();
            }
        }

        private string _Logradouro;
        public string Logradouro
        {
            get => _Logradouro;
            set
            {
                _Logradouro = value;
                OnPropertyChanged();
            }
        }

        private string _Bairro;
        public string Bairro
        {
            get => _Bairro;
            set
            {
                _Bairro = value;
                OnPropertyChanged();
            }
        }

        private string _Cidade;
        public string Cidade
        {
            get => _Cidade;
            set
            {
                _Cidade = value;
                OnPropertyChanged();
            }
        }

        private string _Uf;
        public string Uf
        {
            get => _Uf;
            set
            {
                _Uf = value;
                OnPropertyChanged();
            }

        }
        public bool HasCep { get => _cepDto != null; }

        #endregion

        #region Botão Buscar Endereço

        private Command _BuscarCommand;
        public Command BuscarCommand => _BuscarCommand ?? (_BuscarCommand = new Command(async () => await BuscarCommandExecute()));
        async Task BuscarCommandExecute()
        {
            try
            {
                if (IsBusy)
                    return;

                IsBusy = true;
                BuscarCommand.ChangeCanExecute();
                AdicionarCommand.ChangeCanExecute();

                var locations = await Geocoding.GetLocationsAsync(EnderecoBusca);
                var location = locations?.FirstOrDefault();

                posicaoDestino = new Position(location.Latitude, location.Longitude);
                var endereco = await localizacao.GetEnderecoAsync(posicaoDestino);

                Logradouro = endereco.Thoroughfare;
                Bairro = endereco.SubLocality;
                Cidade = endereco.Locality;
                Uf = endereco.AdminArea;

                _cepDto = new CepDto
                {
                    Id = Guid.NewGuid(),
                    LatitudeDestino = location.Latitude,
                    LongitudeDestino = location.Longitude
                };

                OnPropertyChanged(nameof(HasCep));
            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Erro", e.Message, "OK");
            }
            finally
            {
                IsBusy = false;
                BuscarCommand.ChangeCanExecute();
                AdicionarCommand.ChangeCanExecute();
            }
        }

        #endregion

        #region Adicionar CEP

        private Command _AdicionarCommand;
        public Command AdicionarCommand => _AdicionarCommand ?? (_AdicionarCommand = new Command(async () => await AdicionarCommandExecute()));
        async Task AdicionarCommandExecute()
        {
            try
            {
                if (IsBusy)
                    return;

                IsBusy = true;
                BuscarCommand.ChangeCanExecute();
                AdicionarCommand.ChangeCanExecute();

                _cepDto.LatitudeOrigem = posicaoOrigem.Result.Latitude;
                _cepDto.LongitudeOrigem = posicaoOrigem.Result.Longitude;

                DataBase.Current.Save(_cepDto);

                _cepDto = null;
                _EnderecoBusca = "";

                OnPropertyChanged(nameof(HasCep));
                OnPropertyChanged(nameof(EnderecoBusca));

                await PushAsync(new MapaPage(posicaoOrigem.Result, posicaoDestino, false));

            }
            catch (Exception e)
            {
                await App.Current.MainPage.DisplayAlert("Erro", e.Message, "OK");
            }
            finally
            {
                IsBusy = false;
                BuscarCommand.ChangeCanExecute();
                AdicionarCommand.ChangeCanExecute();
            }
        }

        #endregion

        #region Mostrar historico

        private Command _MostrarHistoricoCommand;

        public Command MostrarHistoricoCommand => _MostrarHistoricoCommand ?? (_MostrarHistoricoCommand =
           new Command(async () => await PushAsync(new HistoricoPage())));

        #endregion

    }
}
