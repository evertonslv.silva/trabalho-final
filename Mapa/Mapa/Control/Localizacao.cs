﻿using Xamarin.Essentials;
using System.Linq;
using System.Threading.Tasks;
using System;
using Xamarin.Forms.GoogleMaps;

namespace Mapa.Control
{
    public class Localizacao
    {
        internal async Task<Placemark> GetEnderecoAsync(Position pos)
        {
            var enderecos = await Geocoding.GetPlacemarksAsync(pos.Latitude, pos.Longitude);
            Placemark endereco = enderecos?.FirstOrDefault();

            return endereco;
        }

        public async Task<Position> GetPosicaoAtualAsinc()
        {
            var request = new GeolocationRequest(GeolocationAccuracy.Medium);
            Location local = await Geolocation.GetLocationAsync(request);
            return new Position(local.Latitude, local.Longitude);

        }

        internal async Task<string> GetDescricaoEnderecoAsync(Position posicao)
        {
            var local = await GetEnderecoAsync(posicao);

            if (local != null)
                return $"{local.Thoroughfare}, {local.SubLocality}, {local.Locality}";

            return "";
        }
    }
}
