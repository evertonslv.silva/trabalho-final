﻿using Mapa.Data.Dtos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.GoogleMaps;

namespace Mapa.Control
{
    class Historico
    {
        public CepDto cep { get; set; }
        private Localizacao localizacao;

        private Task<string> _EnderecoOrigem;
        public string EnderecoOrigem { get => $"Origem: {_EnderecoOrigem.Result}"; }

        private Task<string> _EnderecoDestino;
        public string EnderecoDestino { get => $"Destino: {_EnderecoDestino.Result}"; }

        public Historico(CepDto cp)
        {
            cep = cp;
            localizacao = new Localizacao();
            SetEnderecoOrigem();
            SetEnderecoDestino();
        }

        private void SetEnderecoOrigem()
        {
            _EnderecoOrigem = localizacao.GetDescricaoEnderecoAsync(new Position(cep.LatitudeOrigem, cep.LongitudeOrigem));
            
        }

        private void SetEnderecoDestino()
        {
            _EnderecoDestino = localizacao.GetDescricaoEnderecoAsync(new Position(cep.LatitudeDestino, cep.LongitudeDestino));
        }
    }
}
