﻿using Newtonsoft.Json;

namespace Mapa.Model
{
    public class Localizacao
    {
        [JsonProperty("lat")]
        public double Latitude { get; set; }

        [JsonProperty("Lng")]
        public double Longitude { get; set; }
    }
}