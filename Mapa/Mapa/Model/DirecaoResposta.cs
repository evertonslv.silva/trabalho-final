﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mapa.Model
{
    public class DirecaoResposta
    {
        [JsonProperty("status")]
        private string StatusText { get; set; }

        [JsonProperty("routes")]
        public IEnumerable<ResultadoRota> Rotas { get; set; }
    }
}
