﻿namespace Mapa.Model
{
    public class TextValue
    {
        public double Value { get; set; }

        public string Text { get; set; }
    }
}