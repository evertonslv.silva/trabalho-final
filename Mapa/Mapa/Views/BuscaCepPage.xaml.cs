﻿using Mapa.ViewModels;
using Xamarin.Forms;

namespace Mapa.Views
{
    public partial class BuscaCepPage : ContentPage
	{
		public BuscaCepPage ()
		{
			InitializeComponent ();
            BindingContext = new BuscaCepViewModel();
		}
	}
}