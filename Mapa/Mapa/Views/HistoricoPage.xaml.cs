﻿using Mapa.ViewModels;

using Xamarin.Forms;

namespace Mapa.Views
{
    public partial class HistoricoPage : ContentPage
    {

        public HistoricoPage()
        {
            InitializeComponent();
            BindingContext = new HistoricoViewModel();
        }
    }
}
