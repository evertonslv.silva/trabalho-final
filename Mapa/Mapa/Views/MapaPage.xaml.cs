﻿using Mapa.Clients;
using Mapa.Control;
using Mapa.CustomRender;
using Mapa.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace Mapa.Views
{
    public partial class MapaPage : ContentPage
    {
        public MapaPage(Position origem, Position destino, bool hist)
        {
            InitializeComponent();
            BindingContext = new MapaViewModel(origem, destino, hist);

            MapaViewModel.map = map;
            map.MoveToRegion(MapSpan.FromCenterAndRadius(origem, Distance.FromKilometers(1)));
        }

    }
}