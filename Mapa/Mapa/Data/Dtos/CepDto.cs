﻿using SQLite;
using System;

namespace Mapa.Data.Dtos
{
    [Table("Cep")]

    sealed class CepDto
    {
        [PrimaryKey]
        public Guid Id { get; set; }
        public double LatitudeOrigem { get; set; }
        public double LongitudeOrigem { get; set; }
        public double LatitudeDestino { get; set; }
        public double LongitudeDestino { get; set; }    
    }
}
