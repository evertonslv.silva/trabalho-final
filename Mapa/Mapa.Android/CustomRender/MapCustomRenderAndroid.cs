﻿using System.ComponentModel;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Mapa.CustomRender;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.GoogleMaps.Android;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Mapa.Droid.CustomRender;
using System;
using Mapa.Control;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;
using Position = Xamarin.Forms.GoogleMaps.Position;
using Plugin.Geolocator;

[assembly: ExportRenderer(typeof(MapCustomRender), typeof(MapCustomRenderAndroid))]
namespace Mapa.Droid.CustomRender
{
    class MapCustomRenderAndroid : MapRenderer, IOnMapReadyCallback
    {
        GoogleMap map;
        Android.Gms.Maps.Model.Polyline polyline;
        private IGeolocator geolocator;

        public MapCustomRenderAndroid(Context context) : base(context) { }

        public void OnMapReady(GoogleMap googleMap)
        {
            map = googleMap;
            map.MapType = 1;
            this.Map.MyLocationEnabled = true;
            this.Map.UiSettings.MyLocationButtonEnabled = true;
            this.Map.UiSettings.CompassEnabled = true;

            this.geolocator = CrossGeolocator.Current;

            this.geolocator.DesiredAccuracy = 50;
            this.geolocator.PositionError += OnListeningError;
            this.geolocator.PositionChanged += OnPositionChanged;

            UpdatePolyline();
            Start();
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null) return;

            if (e.NewElement != null)
                ((MapView)Control).GetMapAsync(this);
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (this.Element == null || this.Control == null) return;

            if (e.PropertyName == MapCustomRender.CoordenadasRotaProperty.PropertyName)
            {
                UpdatePolyline();
            }
        }

        private void UpdatePolyline()
        {
            if (polyline != null)
            {
                polyline.Remove();
                polyline.Dispose();
            }

            var polilyneOptions = new PolylineOptions();
            polilyneOptions.InvokeColor(Android.Graphics.Color.Blue);

            var coordenadas = ((MapCustomRender)this.Element).CoordenadasRota;

            foreach (var position in coordenadas)
            {
                polilyneOptions.Add(new LatLng(position.Latitude, position.Longitude));
            }

            polyline = map.AddPolyline(polilyneOptions);
        }

        #region Aqui a magica acontece. Retorna a posição atual
        
        public void Start()
        {
            if (!this.geolocator.IsListening)
            {
                this.geolocator.StartListeningAsync(TimeSpan.FromSeconds(30), 0, includeHeading: true);
            }
            else
            {
                this.geolocator.StopListeningAsync();
            }
        }

        private void OnListeningError(object sender, PositionErrorEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                App.Current.MainPage.DisplayAlert("Error", e.Error.ToString(), "OK");
            });
        }

        private void OnPositionChanged(object sender, PositionEventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                // Ideia de como pode fazer
                // criar um Position na classe que guarda a posição atual
                // chamar esse metodo UpdatePolyline();
                // nesse metodo criar logica para pintar de outra cor

                App.Current.MainPage.DisplayAlert("PositionChanged", $"{e.Position.Timestamp.ToString("G")}" +
                    $"-{e.Position.Latitude.ToString("N4")}" +
                    $"-{e.Position.Longitude.ToString("N4")}", "OK");
            });
        }

        #endregion
    }
}