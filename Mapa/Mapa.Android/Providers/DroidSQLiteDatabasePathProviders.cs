﻿using System.IO;
using Mapa.Droid.Providers;
using Mapa.Providers;
using Xamarin.Forms;
using Environment = System.Environment;

[assembly: Dependency(typeof(DroidSQLiteDatabasePathProviders))]

namespace Mapa.Droid.Providers
{
    class DroidSQLiteDatabasePathProviders : ISQLiteDatabasePathProviders
    {

        public DroidSQLiteDatabasePathProviders() { }

        public string GetDatabasePath()
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "cep.db3");
        }
    }
}