﻿using System;
using System.IO;
using Mapa.iOS.Providers;
using Mapa.Providers;
using Xamarin.Forms;

[assembly: Dependency(typeof(IOSSQLiteDatabasePathProviders))]

namespace Mapa.iOS.Providers
{
    class IOSSQLiteDatabasePathProviders : ISQLiteDatabasePathProviders
    {
        public IOSSQLiteDatabasePathProviders() { }

        public string GetDatabasePath()
        {
            var db = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "..", "Library", "Databases");

            if (!Directory.Exists(db))
                Directory.CreateDirectory(db);

            return Path.Combine(db, "cep.db3");
        }
    }
}